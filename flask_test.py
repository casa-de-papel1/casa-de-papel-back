import os
from flask import Flask, jsonify, request, flash
import json
from pathlib import Path
from werkzeug.utils import redirect, secure_filename
from OtzarMovementsParser import OtzarMovementsParser


def get_json():
    with open("out_files/movements.json") as out:
        data = json.load(out)
        return jsonify(data)
        # return Response(json.dumps(data), status=200)


def upload():
    if request:
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        filename = secure_filename(file.filename)
        file_path = os.path.join('test_files/from_client', filename)
        file.save(file_path)
        parser = OtzarMovementsParser(file_path, "out_files/from_client/" + filename)
        parser.to_json()
        filename_with_json = Path(filename)
        filename_with_json = filename_with_json.with_suffix('.json')
        with open("out_files/from_client/" + str(filename_with_json)) as out:
            data = json.load(out)
            return jsonify(data)
    return 'not'
