from abc import ABC, abstractmethod
import os.path
import pandas as pd
# todo change import here
from prod_config import OtzarMovementsConfig


class Parser(ABC):
    def __init__(self, file_path, out_path):
        if not os.path.exists(file_path):
            raise Exception("File does not exist!")
        self._file_path = file_path
        self._out_path = out_path

    def convert_to_csv(self):
        excel = pd.read_excel(self._file_path)
        csv_file = OtzarMovementsConfig.STAGE_FILE_PATH
        excel.to_csv(csv_file, index=None, header=True)
        return csv_file

    @staticmethod
    def is_blank(val):
        return val in OtzarMovementsConfig.BLANKS

    @abstractmethod
    def is_valid_format(self):
        pass

    @abstractmethod
    def to_json(self):
        pass
