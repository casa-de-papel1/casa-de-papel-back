class OtzarMovementsConfig:
    BLANKS = ['', ' ']
    DATE_END = 10
    DATE_START = 0
    FIRST_MOVEMENT = 2
    FORMATS = ('xls', 'xlsx')
    IMPORTANT_KEYS = ["movement_date", "description", "credit", "debt", "balance"]
    JSON_FILE_PATH = 'out_files/movements/'
    JSON_SUFFIX = '.json'
    KEYS = ["blank", "movement_date", "movement_type", "description",
            "num", "credit", "debt", "credit_date", "balance"]
    MONTH_END = 7
    MOVEMENT_DATE = "movement_date"
    STAGE_FILE_PATH = "out_files/csv_file.csv"


class OtzarExpensesConfig:
    CARD_NUM = "card num"
    CARD_NUM_REGEX = "[0-9][0-9][0-9][0-9]"
    CARD_STRING = "כרטיס"
    DATE = "date"
    DATE_END = 10
    DATE_REGEX = "[0-9][0-9][0-9][0-9][-/][0-9][0-9][-/][0-9][0-9]"
    DATE_START = 0
    FIRST_COL = 1
    FOREIGN_COIN_STR = "במט\"ח"
    FOREIGN_EXPENSES = "foreign expenses"
    FOREIGN_EXPENSE_KEYS = ["date", "business name", "original amount", "coin", "charge amount"]
    FOREIGN_SUM = "foreign sum"
    FOREIGN_SUM_COL = 5
    FORMATS = ('xls', 'xlsx')
    JSON_FILE_PATH = 'out_files/expenses.json'
    LOCAL_EXPENSES = "local expenses"
    LOCAL_EXPENSE_KEYS = ["date", "business name", "deal amount", "charge amount", "description"]
    LOCAL_SUM = "local sum"
    LOCAL_SUM_COL = 4
    MONTH = "month"
    MONTH_REGEX = "[-/][0-9][0-9][-/]"
    SUM_STR = "סה\"כ"


class MainConfig:
    INPUT_FILE_PATH = 'test_files/movements/movements-8.20.xls'
