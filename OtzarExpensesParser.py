from Parser import Parser
from prod_config import OtzarExpensesConfig
import csv
import re
import json


class OtzarExpensesParser(Parser):
    def __init__(self, file_path, out_path):
        super().__init__(file_path, out_path)

    def is_valid_format(self):
        # the parser supports only specific formats
        return self._file_path.lower().endswith(OtzarExpensesConfig.FORMATS)

    @staticmethod
    def extract_card_num(row):
        return re.findall(OtzarExpensesConfig.CARD_NUM_REGEX, row[OtzarExpensesConfig.FIRST_COL])[0]

    @staticmethod
    def extract_month(row):
        with_slashes = re.findall(OtzarExpensesConfig.MONTH_REGEX, row[OtzarExpensesConfig.FIRST_COL])[0]
        return re.findall('[0-9][0-9]', with_slashes)[0]

    @staticmethod
    def is_new_card(row):
        return OtzarExpensesConfig.CARD_STRING in row[OtzarExpensesConfig.FIRST_COL]

    @staticmethod
    def is_date(cell):
        return re.findall(OtzarExpensesConfig.DATE_REGEX, cell)

    @staticmethod
    def is_amount_sum(row):
        return row[OtzarExpensesConfig.FIRST_COL] == OtzarExpensesConfig.SUM_STR

    @staticmethod
    def is_foreign_coin_headline(row):
        return OtzarExpensesConfig.FOREIGN_COIN_STR in row[OtzarExpensesConfig.FIRST_COL]

    @staticmethod
    def remove_hour(expense):
        if OtzarExpensesConfig.DATE not in expense:
            raise Exception("Date is not found!")
        # todo how to make it cleaner?
        expense[OtzarExpensesConfig.DATE] = \
            expense[OtzarExpensesConfig.DATE][OtzarExpensesConfig.DATE_START:OtzarExpensesConfig.DATE_END]
        return expense

    @staticmethod
    def create_local_expense(row):
        i_max = len(OtzarExpensesConfig.LOCAL_EXPENSE_KEYS)
        j_max = OtzarExpensesConfig.FIRST_COL + i_max
        return {OtzarExpensesConfig.LOCAL_EXPENSE_KEYS[i]: row[j] for i, j in
                zip(range(i_max), range(OtzarExpensesConfig.FIRST_COL, j_max))}

    @staticmethod
    def create_foreign_expense(row):
        i_max = len(OtzarExpensesConfig.FOREIGN_EXPENSE_KEYS)
        j_max = OtzarExpensesConfig.FIRST_COL + i_max
        return {OtzarExpensesConfig.FOREIGN_EXPENSE_KEYS[i]: row[j] for i, j in
                zip(range(i_max), range(OtzarExpensesConfig.FIRST_COL, j_max))}

    def create_cards(self):
        if not self.is_valid_format():
            raise Exception("File's format is not valid!")

        csv_file = super().convert_to_csv()

        # create expenses list
        cards = []
        with open(csv_file) as csv_opened:
            csv_reader = csv.reader(csv_opened)
            card = {}
            is_foreign_expense = False
            for row in csv_reader:
                if self.is_foreign_coin_headline(row):
                    is_foreign_expense = True
                    foreign_expenses = []
                if self.is_new_card(row):
                    # if the new card is not the first, add the last to list
                    if card:
                        cards.append(card)
                    # if there is a new card, so the default expenses are local
                    is_foreign_expense = False
                    # initiate a new card with its num and a new expenses list
                    card_num = self.extract_card_num(row)
                    card = {OtzarExpensesConfig.CARD_NUM: card_num}
                    # add card's month
                    month = self.extract_month(row)
                    card[OtzarExpensesConfig.MONTH] = month
                    local_expenses = []
                    continue
                # if the first column is a date then the row is an expense
                if self.is_date(row[OtzarExpensesConfig.FIRST_COL]) and not is_foreign_expense:
                    local_expense = self.create_local_expense(row)
                    local_expense = self.remove_hour(local_expense)
                    local_expenses.append(local_expense)
                    continue
                # if the first column is a date then the row is an expense
                if self.is_date(row[OtzarExpensesConfig.FIRST_COL]) and is_foreign_expense:
                    foreign_expense = self.create_foreign_expense(row)
                    foreign_expense = self.remove_hour(foreign_expense)
                    foreign_expenses.append(foreign_expense)
                    continue
                if self.is_amount_sum(row) and not is_foreign_expense:
                    card[OtzarExpensesConfig.LOCAL_SUM] = row[OtzarExpensesConfig.LOCAL_SUM_COL]
                    card[OtzarExpensesConfig.LOCAL_EXPENSES] = local_expenses
                    continue
                if self.is_amount_sum(row) and is_foreign_expense:
                    card[OtzarExpensesConfig.FOREIGN_SUM] = row[OtzarExpensesConfig.FOREIGN_SUM_COL]
                    card[OtzarExpensesConfig.FOREIGN_EXPENSES] = foreign_expenses
                    continue
            cards.append(card)
            return cards

    def to_json(self):
        expenses = self.create_cards()
        with open(OtzarExpensesConfig.JSON_FILE_PATH, 'w') as json_file:
            json.dump(expenses, json_file, ensure_ascii=False)
        return OtzarExpensesConfig.JSON_FILE_PATH


def main():
    parser = OtzarExpensesParser("test_files/expenses/expenses-8.20.xls")
    parser.to_json()


if __name__ == '__main__':
    main()
