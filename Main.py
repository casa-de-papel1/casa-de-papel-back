from OtzarMovementsParser import OtzarMovementsParser
from flask import Flask
from flask_cors import CORS
import flask_test
from prod_config import MainConfig


def main():
    # parser = OtzarMovementsParser(MainConfig.INPUT_FILE_PATH)
    # parser.to_json()

    app = Flask(__name__)
    CORS(app)
    # app.add_url_rule('/', view_func=flask_test.get_json)
    app.add_url_rule('/', view_func=flask_test.upload, endpoint='/', methods=['GET', 'POST'])
    app.run()


if __name__ == "__main__":
    main()
