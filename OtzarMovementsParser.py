import csv
import json
from pathlib import Path
from Parser import Parser
from prod_config import OtzarMovementsConfig


class OtzarMovementsParser(Parser):
    def __init__(self, file_path, out_path):
        super().__init__(file_path, out_path)

    def is_valid_format(self):
        # the parser supports only specific formats
        return self._file_path.lower().endswith(OtzarMovementsConfig.FORMATS)

    def dict_filter(self, row):
        # todo to dict comprehension
        dict1 = {key: val for key, val in row.items() if key in OtzarMovementsConfig.IMPORTANT_KEYS}
        is_all_blank = True
        for val in dict1.values():
            if not super().is_blank(val):
                is_all_blank = False
        if is_all_blank:
            return {}
        return dict1

    @staticmethod
    def remove_hour(movement):
        if OtzarMovementsConfig.MOVEMENT_DATE not in movement:
            raise Exception("Date is not found!")
        # todo how to make it cleaner?
        movement[OtzarMovementsConfig.MOVEMENT_DATE] = \
            movement[OtzarMovementsConfig.MOVEMENT_DATE][OtzarMovementsConfig.DATE_START:OtzarMovementsConfig.DATE_END]
        return movement

    def create_movements(self):
        if not self.is_valid_format():
            raise Exception("File's format is not valid!")

        csv_file = super().convert_to_csv()
        # create and update movements list
        movements = []
        with open(csv_file) as csv_opened:
            csv_reader = csv.DictReader(csv_opened, fieldnames=OtzarMovementsConfig.KEYS)
            counter = 0
            is_movement = False
            for row in csv_reader:
                movement = self.dict_filter(row)
                if row['description'] == 'יתרת פתיחה':
                    is_movement = True
                    continue
                if is_movement:
                    # remove hour from date
                    movement = self.remove_hour(movement)
                    movements.append(movement)
                counter += 1
        return movements

    @staticmethod
    def extract_month(movements):
        return (movements[0].get(OtzarMovementsConfig.MOVEMENT_DATE)
                [OtzarMovementsConfig.DATE_START:OtzarMovementsConfig.MONTH_END])

    def to_json(self):
        movements = self.create_movements()
        month = self.extract_month(movements)
        out_path = Path(self._out_path)
        out_path = out_path.with_suffix(OtzarMovementsConfig.JSON_SUFFIX)
        with open(out_path, 'w') as json_file:
            json.dump(movements, json_file, ensure_ascii=False)
